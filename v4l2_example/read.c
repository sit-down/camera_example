//使用摄像头捕获视频，将每一帧图像保存到文件

#include <stdio.h> //perror
//open
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <unistd.h>          //close
#include <sys/ioctl.h>       //ioctl
#include <stdlib.h>          //EXIT_FAILURE
#include <linux/videodev2.h> //V4L2
#include <string.h>          //memset
#include <sys/mman.h>        //mmap

int main()
{
    //打开设备文件，需要读写权限
    int fd = open("/dev/video0", O_RDWR);
    if (fd < 0)
    {
        perror("open");
        return EXIT_FAILURE;
    }

    //获取设备能力(设备支持的操作)
    struct v4l2_capability cap;
    if (ioctl(fd, VIDIOC_QUERYCAP, &cap) < 0)
    {
        perror("VIDIOC_QUERYCAP");
        return EXIT_FAILURE;
    }

    //是否支持视频捕获
    if (!(cap.capabilities & V4L2_CAP_VIDEO_CAPTURE))
    {
        fprintf(stderr, "The device does not handle single-planar video capture.\n");
        return EXIT_FAILURE;
    }

    //是否支持读写API
    if (!(cap.capabilities & V4L2_CAP_READWRITE))
    {
        fprintf(stderr, "The device does not support read/write method.\n");
        return EXIT_FAILURE;
    }

    //获取默认图像格式
    struct v4l2_format format;
    format.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;

    if (ioctl(fd, VIDIOC_G_FMT, &format) < 0)
    {
        perror("VIDIOC_G_FMT");
        return EXIT_FAILURE;
    }

    printf("default format: %c%c%c%c %dx%d\n",
           format.fmt.pix.pixelformat & 0xff,
           format.fmt.pix.pixelformat >> 8 & 0xff,
           format.fmt.pix.pixelformat >> 16 & 0xff,
           format.fmt.pix.pixelformat >> 24 & 0xff,
           format.fmt.pix.width,
           format.fmt.pix.height);

    //获取一帧图像的大小
    //未压缩图像大小
    unsigned int frame_size = format.fmt.pix.bytesperline * format.fmt.pix.height;
    if (format.fmt.pix.bytesperline == 0)
    {
        //压缩图像大小
        frame_size = format.fmt.pix.sizeimage;
    }
    printf("frame size = %d bytes\n", frame_size);
    char* buffer = malloc(frame_size);
    if (!buffer)
    {
        perror("malloc");
        return EXIT_FAILURE;
    }

    //帧数
    int frame = 0;

    while (1)
    {
        printf("frame %d\n", frame);
        //等待一帧图像捕获完成后将缓冲区出队（默认阻塞）
        if (read(fd, buffer, frame_size) < 0)
        {
            perror("read");
            break;
        }
        //保存图像到文件
        char filename[20];

        sprintf(filename, "frame%03d.raw", frame++);

        int rawfile = open(filename, O_WRONLY | O_CREAT, 0660);
        if (rawfile < 0)
        {
            perror("open");
            break;
        }

        write(rawfile, buffer, frame_size);
        close(rawfile);
    }

    close(fd);
    return EXIT_SUCCESS;
}
